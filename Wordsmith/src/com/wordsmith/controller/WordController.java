package com.wordsmith.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class WordController {

	@RequestMapping("/index")
	public String index()
	{
		return "/WEB-INF/views/wordsmithIndex.jsp";
	}
	
}
