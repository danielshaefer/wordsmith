package com.wordsmith.dao;

import java.io.File;
import java.io.IOException;
import java.util.Collection;
import java.util.List;

import javax.servlet.ServletContext;
import javax.sql.DataSource;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.apache.commons.io.filefilter.FalseFileFilter;
import org.apache.commons.io.filefilter.TrueFileFilter;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.Resource;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.web.context.support.ServletContextResource;

/**
 * This class provides a way to initialize and shutdown a database.  To initialize, it makes use
 * of the fact that it implements InitializingBean.  To shutdown the database, it implements
 * ApplicationListener and listens for a context shutdown.  We need the servlet context to
 * grab the SQL used in initialization, hence the ServletContextAware.
 */
public class ApplicationInitializer  implements InitializingBean {
	@Autowired DataSource dataSource;
	@Autowired ServletContext servletContext;
	@Value("${app.version}") final String version = "0.0.0";

	List<String> locationList;
	
	Logger log = Logger.getLogger(this.getClass());


	/** InitializingBean implementation */
	public void afterPropertiesSet() throws Exception {
		if (this.dataSource == null) {
			throw new IllegalArgumentException("DataSource is required");
		}
		 //Get the jvm heap size.
        long heapSize = Runtime.getRuntime().totalMemory();
         
        //Print the jvm heap size.
        System.out.println("Heap Size = " + heapSize);
        try
        {
        	initializeDB();
        }
        catch(Throwable t)
        {
        	t.printStackTrace();
        }
	}

	/** Initialize the database by grabbing the SQL and executing it! */
	private void initializeDB() throws IOException 
	{
		if (getLocationList() != null)
		{
			log.info("Attempting to retrieve initializing SQL FROM LIST");
			List<String> locations = getLocationList();
			JdbcTemplate jt = new JdbcTemplate(this.dataSource);
			
			Resource createDir = new ServletContextResource(this.servletContext, "/WEB-INF/sql/create");
			executeSql(createDir);
			Resource insertDir = new ServletContextResource(this.servletContext, "/WEB-INF/sql/insert");
			executeSql(insertDir);
//			for (String string : locations) {
//				System.out.println("Location: " + string);
//				Resource sql = new ServletContextResource(this.servletContext, string);
//				String sqlStr = IOUtils.toString(sql.getInputStream());
//				//System.out.println("QUERY: "  + sqlStr);
//				jt.execute(sqlStr);
//				System.out.println("Success on execution");
//				log.info("Successfully updated SQL");
//			}
		}
		else
			log.info("No location set, so not retrieving initializing SQL");
			
	}

	public void executeSql(Resource dir) throws IOException
	{
		JdbcTemplate jt = new JdbcTemplate(this.dataSource);
		Collection<File> sqlFile = FileUtils.listFiles(dir.getFile(), TrueFileFilter.INSTANCE, FalseFileFilter.INSTANCE);
		for (File file : sqlFile) {
			System.out.println("Executing file: " + file.getName());
			String fileStr = FileUtils.readFileToString(file);
			jt.execute(fileStr);
		}
	}
	
	public List<String> getLocationList() {
		return locationList;
	}

	public void setLocationList(List<String> locationList) {
		this.locationList = locationList;
	}

	public String getVersion() {
		return version;
	}

	public void setDataSource(DataSource dataSource) {
		this.dataSource = dataSource;
	}

	public void setServletContext(ServletContext servletContext) {
		this.servletContext = servletContext;
	}

	
	
}
