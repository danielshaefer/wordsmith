groups {
	jqueryCore {
		js "/scripts/jquery/jquery-1.9.1.js"
		js "/scripts/jquery/jquery-ui-1.10.2.js"
	}
	jqueryAll {
		jqueryCore()
		js "/scripts/jquery/jquery-layout-latest.min.js"
		js "/scripts/jquery/blockui.js"
		js "/scripts/jquery/jquery.json-2.3.min.js"
	}
	jqueryCssCore {
		css "/css/normalize.min.css"
		css "/css/jquery-ui-smoothness-1.10.2.css"
	}
	select2 {
		js "/scripts/select2.js"
		css "/css/select2/select2.css"
	}
	
  }